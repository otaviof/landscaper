## VARIABLES
APP=landscaper
FOLDERS=./cmd/... ./pkg/...

BUILD_DIR ?= build
DOCKER_BUILD_IMAGE ?= registry-eng.gitlab.schubergphilis.com/k8s/builder-go
LD_RELEASE_FLAGS ?= -w -linkmode external -extldflags '-static'

# get version info from git's tags
GIT_COMMIT := $(shell git rev-parse HEAD)
GIT_TAG := $(shell git describe --tags --long --dirty 2>/dev/null)
VERSION := $(shell git describe --tags --abbrev=0 2>/dev/null)

# inject version info into version vars
LD_RELEASE_FLAGS += -X eng.gitlab.schubergphilis.com/k8s/landscaper/pkg/landscaper.GitCommit=${GIT_COMMIT}
LD_RELEASE_FLAGS += -X eng.gitlab.schubergphilis.com/k8s/landscaper/pkg/landscaper.GitTag=${GIT_TAG}
LD_RELEASE_FLAGS += -X eng.gitlab.schubergphilis.com/k8s/landscaper/pkg/landscaper.SemVer=${VERSION}

.PHONY: test update clean check binary binary_in_container docker_build_app help

default: build

update: ## Update the vendored Go dependencies
	@glide update -v --resolve-current

clean: ## Clean the project
	@go clean
	@rm -rf $(BUILD_DIR)

check: ## Checks codestyle and correctness
	@which gometalinter >/dev/null; if [ $$? -eq 1 ]; then \
		go get -v -u github.com/alecthomas/gometalinter; \
		gometalinter --install --update; \
	fi
	gometalinter --vendor --disable-all --enable=vet --enable=golint $(FOLDERS)

test: ## Tests the project
	@go test -cover $(FOLDERS)

build: binary build_image

binary: ## Builds the Go binary for our app
	docker pull $(DOCKER_BUILD_IMAGE)
	docker run \
		-v $(CURDIR):/go/src/eng.gitlab.schubergphilis.com/k8s/$(APP) \
		--rm --workdir=/go/src/eng.gitlab.schubergphilis.com/k8s/$(APP) \
		$(DOCKER_BUILD_IMAGE) \
		sh -c "make binary_in_container"

binary_in_container:
	cd cmd && CC=/usr/bin/x86_64-alpine-linux-musl-gcc go build -ldflags "$(LD_RELEASE_FLAGS)" -o ../$(BUILD_DIR)/$(APP)

build_image:
	docker build -t registry-eng.gitlab.schubergphilis.com/k8s/$(APP) .
	docker push registry-eng.gitlab.schubergphilis.com/k8s/$(APP)

## MAKE HELP
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}'
