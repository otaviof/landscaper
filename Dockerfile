FROM alpine

MAINTAINER Eneco Red Team <int-eet@schubergphilis.com>

RUN echo '@edge http://nl.alpinelinux.org/alpine/edge/main' >> /etc/apk/repositories && \
    echo '@community http://nl.alpinelinux.org/alpine/edge/community' >> /etc/apk/repositories && \
    apk upgrade --update && \
    apk add bash curl ca-certificates git

ADD kubectl/* /etc/kubectl/

COPY build/landscaper /usr/bin/landscaper

ENV HELM_VERSION="v2.0.0" \
    HELM_HOME_DIR="/opt/helm" \
    HELM_PRIV_REPO_URL="https://eng.gitlab.schubergphilis.com/k8s/repo/raw/master/packages"

ENV KUBE_VERSION="1.4.6" \
    KUBE_FOLDER="/root/.kube"

RUN wget http://storage.googleapis.com/kubernetes-helm/helm-${HELM_VERSION}-linux-amd64.tar.gz && \
    mkdir -v -p ${HELM_HOME_DIR}-${HELM_VERSION} && \
    tar zxvpf helm-${HELM_VERSION}-linux-amd64.tar.gz -C ${HELM_HOME_DIR}-${HELM_VERSION} && \
    ln -s ${HELM_HOME_DIR}-${HELM_VERSION} ${HELM_HOME_DIR} && \
    ln -s /opt/helm/linux-amd64/helm /usr/bin/helm && \
    rm -f helm-${HELM_VERSION}-linux-amd64.tar.gz

# configuring Helm Repositories
RUN helm init --client-only && \
    helm repo add eet ${HELM_PRIV_REPO_URL}

RUN mkdir -v -p /root/.kube
RUN ln -s /etc/kubectl/bootstrap.sh /usr/bin/bootstrap

CMD bash

WORKDIR /root
